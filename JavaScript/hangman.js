//Hangman game for learning numbers in Finnish
//2020-09-25 File created. 2020-10-20 Last modified. 
//Isis Herrero López

const white_font = "\x1b[37m";
const yellow_font = "\x1b[33m"
const red_font = "\x1b[31m";
const green_font = "\x1b[92m";
const ending_font = "\x1b[89m";

const available_words = ["nolla", "yksi", "kaksi", "kolme", "neljä", "viisi", "kuusi", "seitsemän", "kahdeksan", "yhdeksän", "kymmenen"];

function select_word() {
    const random_number = Math.floor(Math.random() * available_words.length);
    const selected_word = available_words[random_number]; 
    return selected_word;
} 

function is_number(string) {
    return /\d/.test(string);
}

function is_letter(string) {
    const letters = /[a-zA-ZäöÄÖ]/g;
    if (letters.test(string)) {
        return true;
    } else {
        return false;
    }
}

function write_lines(word) {
    process.stdout.write("     ");
    for (let index = 0; index < word.length; index++) {
        process.stdout.write(yellow_font + "___ " + ending_font);
    }
}

function write_guessed_letters(word, correct_spaces) {
    process.stdout.write("\n\n     ");
    for (let index = 0; index < word.length; index++) {
        for (let spaces_index = 0; spaces_index < correct_spaces.length; spaces_index++) {
            if (index === correct_spaces[spaces_index]) {
                process.stdout.write(" " + word[index] + "  ");
                break;
            } else if (spaces_index === (correct_spaces.length - 1)) {
                process.stdout.write("    ");
            }
        }
    }
}

function hangman_drawing(errors_count) {
    process.stdout.write(red_font + "\nNew line drawn!" + ending_font);
    for (let index = 0; index <= 10; index++) {
        if (errors_count >= 1) {
            if (errors_count === 1 && index === 9) {
                process.stdout.write("\n__ __");
            } else if (errors_count >= 2 && index === 9) {
                process.stdout.write("\n__|__");
            } else if (errors_count >= 2 && index === 8) {
                process.stdout.write("\n  |");
            } else if (errors_count >= 3 && index === 2) {
                process.stdout.write("\n  +--------+");
            } else if (errors_count === 4 && index === 3) {
                process.stdout.write("\n  |/");
            } else if (errors_count >= 5 && index === 3) {
                process.stdout.write("\n  |/       |");
            } else if (errors_count >= 6 && index === 4) {
                process.stdout.write("\n  |        O");
            } else if (errors_count === 7 && index === 5) {
                process.stdout.write("\n  |        |");
            } else if (errors_count === 8 && index === 5) {
                process.stdout.write("\n  |       /|");
            } else if (errors_count >= 9 && index === 5) {
                process.stdout.write("\n  |       /|\\");
            } else if (errors_count >= 10 && index === 6) {
                process.stdout.write("\n  |        -");
            } else if (errors_count === 11 && index === 7) {
                process.stdout.write("\n  |       /");
            } else if (errors_count === 12 && index === 7) {
                process.stdout.write("\n  |       / \\");
            } else if (errors_count >= 2 && index >= 3 && index < 8) {
                process.stdout.write("\n  |");
            } else {
                process.stdout.write("\n");
            } 
        }
    }
}

process.stdout.write(white_font + "\nWelcome to Hangman!" 
                                + "\nLearn the Finnish numbers from 0 to 10."
                                + "\nHere you have your first word.\n\n"
                                + ending_font);

let word = select_word();
write_lines(word);

process.stdout.write(white_font + "\n\nWhich letter is your guess?: " + ending_font);

let used_letters = [];
let correct_spaces = [];
let errors_count = 0;

let end_of_game = false;

process.stdin.on("data", function(letter_from_keyboard) {
    if (end_of_game === false) {
        const selected_letter = letter_from_keyboard.toString().trim();
        process.stdout.write("\n");
        if (selected_letter.length > 1) {
            process.stdout.write("Please, enter only one letter: ");
        } else if (selected_letter.length === 0) {
            process.stdout.write("Please, enter a letter: ");
        } else if (is_number(letter_from_keyboard) === true) {
            process.stdout.write("No numbers allowed. Please, enter a letter: ");
        } else if (is_letter(letter_from_keyboard) === false) {
            process.stdout.write("Only letters allowed. Please, enter a letter: ");
        } else {
            let new_letter = true;
            for (let index = 0; index < used_letters.length; index++) {
                if (selected_letter === used_letters[index] || selected_letter.toLowerCase() === used_letters[index]) {
                    new_letter = false;
                    break;
                }
            }
            if (new_letter === false) {
                process.stdout.write("You have already used that letter." 
                + "\nHere is the list of the letters you have already used: \n  ");
                for (let index = 0; index < used_letters.length; index++) {
                    if (index === (used_letters.length - 1)) {
                        process.stdout.write(green_font + " " + used_letters[index] + ending_font);
                    } else {
                        process.stdout.write(green_font + " " + used_letters[index] + " " + ending_font);
                    }
                }
                process.stdout.write(white_font + "\nPlease, enter a new letter: " + ending_font);
            } else if (new_letter === true) {
                used_letters.push(selected_letter.toLowerCase());
                let guess;
                for (let index = 0; index <= word.length; index++) {
                    if (selected_letter === word[index] || selected_letter.toLowerCase() === word[index]) {
                        guess = true;
                        correct_spaces.push(index);
                    }
                }
                if (guess === true) {
                    process.stdout.write("\nVery well! The word has the letter " + green_font + selected_letter + ending_font + white_font + "." + ending_font);
                    write_guessed_letters(word, correct_spaces);
                    process.stdout.write("\n");
                    write_lines(word);
                    if (correct_spaces.length === word.length) {
                        process.stdout.write(green_font + "\n\nHURRAY!! You guessed the word before getting hanged!" + ending_font);
                        end_of_game = true;
                        process.stdout.write(white_font + "\n\n\nDo you want to play again? (Y/N)" + ending_font);
                    } else {
                        process.stdout.write(white_font + "\n\nPlease, enter a new letter: " + ending_font);
                    }
                } else {
                    errors_count++;
                    process.stdout.write("\nSorry! There's no " + green_font + selected_letter + ending_font + white_font + " in the word." + ending_font);
                    hangman_drawing(errors_count);
                    if (errors_count < 12) {
                        process.stdout.write(white_font + "\n\n" + ending_font);
                        if (correct_spaces.length === 0) {
                            write_lines(word);
                        } else {
                            write_guessed_letters(word, correct_spaces);
                            process.stdout.write("\n");
                            write_lines(word);
                        }
                        process.stdout.write(white_font + "\n\nPlease, enter a new letter: " + ending_font);
                    } else {
                        process.stdout.write(red_font + "\n\nOh, no! You got hanged!" + ending_font);
                        process.stdout.write(white_font + "\nThe word was: \n\n     " + ending_font);
                        for (let index = 0; index < word.length; index++) {
                            process.stdout.write(" " + word[index] + "  ");
                        }
                        process.stdout.write("\n");
                        write_lines(word);
                        end_of_game = true;
                        process.stdout.write(white_font + "\n\n\nDo you want to play again? (Y/N)" + ending_font);
                    }
                }
            } 
        }
    } else if (end_of_game === true) {
        const answer = letter_from_keyboard.toString().trim().charAt(0).toLowerCase();
        if (answer === "y") {
            used_letters = [];
            correct_spaces = [];
            errors_count = [];
            process.stdout.write("\nGreat! Let's continue playing!" 
                                + "\nHere you have another word.\n\n");
            word = select_word();
            write_lines(word);
            process.stdout.write(white_font + "\n\nWhich letter is your guess?: " + ending_font);
            end_of_game = false;
        } else if (answer === "n") {
            process.exit();
        } else {
            process.stdout.write(white_font + "\n\n\nDo you want to play again? (Y/N)" + ending_font);
        }
    }
});

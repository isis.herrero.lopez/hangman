﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace exercises3
{
    class Program
    {
        static string SelectWord(string[] words) 
        {
            Random randomNumber = new Random();
            int myRandomNumber = randomNumber.Next(1, words.Length);
            string selectedWord = words[myRandomNumber];
            return selectedWord;
        }

        static void writeLines(string word)
        {
            Console.Write("     ");
            for (int index = 0; index < word.Length; index++)
            {
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                Console.Write("___ ");
            }
            Console.ForegroundColor = ConsoleColor.White;
        }

        static bool isLetter(string letter)
        {
            string letters = "^[a-zA-ZäöÄÖ]$";
            Regex reg = new Regex(letters);
            if (reg.IsMatch(letter)) {
                return true;
            } else {
                return false;
            }
        }

        static void writeGuessedLetters(string word, List<int> spaces)
        {
            Console.Write("\n\n     ");
            for (int index = 0; index < word.Length; index++)
            {
                for (int spacesIndex = 0; spacesIndex < spaces.Count; spacesIndex++)
                {
                    if (index == spaces[spacesIndex])
                    {
                        Console.Write(" " + word[index] + "  ");
                        break;
                    }
                    else if (spacesIndex == (spaces.Count - 1))
                    {
                        Console.Write("    ");
                    }
                }
            }
            Console.WriteLine("");
            Console.ForegroundColor = ConsoleColor.White;
        }

        static void hangmanDrawing(int errors)
        {   
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("\nNew line drawn!");
            for (int index = 0; index <= 10; index++)
            {
                if (errors >= 1) 
                {
                    if (errors == 1 && index == 9)
                    {
                        Console.Write("\n__ __");
                    } 
                    else if (errors >= 2 && index == 9)
                    {
                        Console.Write("\n__|__");
                    }
                    else if (errors >= 2 && index == 8)
                    {
                        Console.Write("\n  |");
                    }
                    else if (errors >= 3 && index == 2)
                    {
                        Console.Write("\n  +--------+");
                    }
                    else if (errors == 4 && index == 3)
                    {
                        Console.Write("\n  |/");
                    }
                    else if (errors >= 5 && index == 3)
                    {
                        Console.Write("\n  |/       |");
                    }
                    else if (errors >= 6 && index == 4)
                    {
                        Console.Write("\n  |        O");
                    }
                    else if (errors == 7 && index == 5)
                    {
                        Console.Write("\n  |        |");
                    }
                    else if (errors == 8 && index == 5)
                    {
                        Console.Write("\n  |       /|");
                    }
                    else if (errors >= 9 && index == 5)
                    {
                        Console.Write("\n  |       /|\\");
                    }
                    else if (errors >= 10 && index == 6)
                    {
                        Console.Write("\n  |        -");
                    }
                    else if (errors == 11 && index == 7)
                    {
                        Console.Write("\n  |       /");
                    }
                    else if (errors == 12 && index == 7)
                    {
                        Console.Write("\n  |       / \\");
                    } 
                    else if (errors >= 2 && index >= 3 && index < 8)
                    {
                        Console.Write("\n  |");
                    }
                    else {
                        Console.Write("\n");
                    } 
                }
            }
        }

        static void Main(string[] args)
        {
            string[] availableWords = {"nolla", "yksi", "kaksi", "kolme", "neljä", "viisi", "kuusi", "seitsemän", "kahdeksan", "yhdeksän", "kymmenen"};

            List<char> usedLetters = new List<char>();
            List<int> correctSpaces = new List<int>();
            int errorsCount = 0;

            bool playing = true;
            bool endOfGame = false;

            Console.WriteLine("\nWelcome to Hangman!" 
                                + "\nLearn the Finnish numbers from 0 to 10."
                                + "\nHere you have your first word.\n\n");

            string word = SelectWord(availableWords);
            writeLines(word); 
            Console.Write("\n\nWhich letter is your guess?: ");
            string input = Console.ReadLine();
            
            while (playing == true)
            {
                while (endOfGame == false) 
                {
                    Console.Write("\n");
                    if (input.Length > 1)
                    {
                        Console.Write("Please, enter only one letter: ");
                        input = Console.ReadLine();
                    } 
                    else if (input.Length == 0)
                    {
                        Console.Write("Please, enter a letter: ");
                        input = Console.ReadLine();
                    }
                    else
                    {
                        bool isNumeric = int.TryParse(input, out _);
                        
                        if (isNumeric == true) {
                            Console.Write("No numbers allowed. Please, enter a letter: ");
                            input = Console.ReadLine();
                        } else if (isLetter(input) == false) {
                            Console.Write("Only letters allowed. Please, enter a letter: ");
                            input = Console.ReadLine();
                        } else {
                            char selectedLetter = Convert.ToChar(input);
                            bool newLetter = true;
                            for (int index = 0; index < usedLetters.Count; index++)
                            {
                                if (selectedLetter == usedLetters[index] || char.ToLower(selectedLetter) == usedLetters[index])
                                {
                                    newLetter = false;
                                    break;
                                }
                            }

                            if (newLetter == false) {
                                Console.WriteLine("You have already used that letter." 
                                            + "\nHere is the list of the letters you have already used: \n  ");
                                Console.ForegroundColor = ConsoleColor.Green;
                                for (int index = 0; index < usedLetters.Count; index++)
                                {
                                    if (index == (usedLetters.Count - 1))
                                    {
                                        Console.Write(" " + usedLetters[index]);
                                    }
                                    else 
                                    {
                                        Console.Write(" " + usedLetters[index] + " ");
                                    }
                                }
                                Console.ForegroundColor = ConsoleColor.White;
                                Console.Write("\n\nPlease, enter a new letter: ");
                                input = Console.ReadLine();
                            } 
                            else
                            {
                                usedLetters.Add(selectedLetter);
                                bool guess = false;
                                for (int index = 0; index < word.Length; index++)
                                {
                                    if (selectedLetter == word[index] || char.ToLower(selectedLetter) == word[index])
                                    {
                                        guess = true;
                                        correctSpaces.Add(index);
                                    }
                                }
                                if (guess == true) {
                                    Console.Write("\nVery well! The word has the letter ");
                                    Console.ForegroundColor = ConsoleColor.Green;
                                    Console.Write(selectedLetter);
                                    Console.ForegroundColor = ConsoleColor.White;
                                    Console.WriteLine(".");

                                    writeGuessedLetters(word, correctSpaces);
                                    writeLines(word);
                                    
                                    if (correctSpaces.Count == word.Length)
                                    {
                                        Console.ForegroundColor = ConsoleColor.Green;
                                        Console.WriteLine("\n\nHURRAY!! You guessed the word before getting hanged!");
                                        Console.ForegroundColor = ConsoleColor.White;
                                        endOfGame = true;
                                        Console.Write("\n\n\nDo you want to play again? (Y/N) ");
                                        input = Console.ReadLine();
                                    }
                                    else
                                    {
                                        Console.Write("\n\nPlease, enter a new letter: ");
                                        input = Console.ReadLine();
                                    }
                                }
                                else 
                                {
                                    errorsCount++;
                                    Console.Write("\nSorry! There's no ");
                                    Console.ForegroundColor = ConsoleColor.Green;
                                    Console.Write(selectedLetter);
                                    Console.ForegroundColor = ConsoleColor.White;

                                    Console.WriteLine(" in the word.");
                                    hangmanDrawing(errorsCount);

                                    if (errorsCount < 12)
                                    {
                                        Console.WriteLine("\n\n");
                                        if (correctSpaces.Count == 0)
                                        {
                                            writeLines(word);
                                        }
                                        else 
                                        {
                                            writeGuessedLetters(word, correctSpaces);
                                            writeLines(word);
                                        }
                                        Console.Write("\n\nPlease, enter a new letter: ");
                                        input = Console.ReadLine();
                                    }
                                    else
                                    {
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("\n\nOh, no! You got hanged!");
                                        Console.ForegroundColor = ConsoleColor.White;
                                        Console.Write("\nThe word was: \n\n     ");
                                        for (int index = 0; index < word.Length; index++)
                                        {
                                            Console.Write(" " + word[index] + "  ");
                                        }
                                        Console.Write("\n");
                                        writeLines(word);
                                        endOfGame = true;
                                        Console.Write("\n\n\nDo you want to play again? (Y/N) ");
                                        input = Console.ReadLine();
                                    }
                                }
                            } 

                        }
                    }
                }

                while (endOfGame == true)
                {
                    if (input == "y" || input.ToLower() == "y")
                    {
                        usedLetters.Clear();
                        correctSpaces.Clear();
                        errorsCount = 0;
                        Console.WriteLine("\nGreat! Let's continue playing!" 
                                        + "\nHere you have another word.\n");
                        word = SelectWord(availableWords);
                        writeLines(word);
                        Console.Write("\n\nWhich letter is your guess?: ");
                        input = Console.ReadLine();
                        endOfGame = false;
                    }
                    else if (input == "n" || input.ToLower() == "n")
                    {
                        playing = false;
                        Environment.Exit(0);
                    }
                    else
                    {
                    Console.Write("\nDo you want to play again? (Y/N) ");
                    input = Console.ReadLine();
                    }
                }
            }
        }
    }
}
